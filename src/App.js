import React, {Component} from 'react';
import './App.css';
import Header from './components/Header'
import Menu from './components/Menu'
import Section from './components/Section'
import PageContent from './components/PageContent'
import Sidebar from './components/Sidebar'
import Footer from './components/Footer'

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      sections: [
        {number:'1', class:'section-skin-blue'},
        {number:'2', class:'section-skin-gray'},
        {number:'3', class:'section-skin-pink'},
      ]
    }
  }
  render(){
    return (
      <div className="App">
      <div class="grid-container">
        <Header></Header>
        <Menu></Menu>
        {this.state.sections.map( section =>  <Section number={section.number} class={section.class}></Section>)}
        <PageContent></PageContent>
        <Sidebar></Sidebar>
        <Footer></Footer>
      </div>
      </div>
    );
  }
}

export default App;
