import React, { Component } from 'react';

class Section extends Component {
  getClass = `section ${this.props.class}`;
  render(){
    return (
        <React.Fragment>
            <div className={this.getClass}>Section {this.props.number}</div>
        </React.Fragment>
    );
  }
}

export default Section;
